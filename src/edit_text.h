/*OOPARTS OOPARTS OOPARTS OOPARTS OOPARTS OOPARTS OOPARTS
=========================================================
	edit_text.h

	EditText class의 header file입니다.
=======================================================*/

#pragma once

#include <SFML\Graphics.hpp>

using namespace sf;

class EditText : public Text {
/*	입력용 text */

public:
							  EditText(const Font & font, size_t char_size);
							 ~EditText();

	void					  setCharSize(size_t char_size);
	void					  setCaretColor(const Color &color) {_caret.setFillColor(color); }
	void					  setPosition(float x, float y) {Text::setPosition(x, y); updateCaret(); }

	bool					  handleEnteredKey(wchar_t unicode);
	bool					  handleKeyPress(Event::KeyEvent key);

	void					  setDrawCaret(bool b) { _draw_caret = b; }

	const String &			  getString() {return _string;}

	void					  clear() {_string.clear(); setString(""); _caret_pos = 0U; updateCaret(); }

private:
	virtual void draw(RenderTarget& target, RenderStates states) const;

private:
	String			_string;
	size_t			_caret_pos;
	
	RectangleShape	_caret;
	bool			_draw_caret;

	void			updateCaret();
};