/*OOPARTS OOPARTS OOPARTS OOPARTS OOPARTS OOPARTS OOPARTS
=========================================================
	protocol.h

	server와 client간의 통신 규약을 정했습니다.
=======================================================*/

#pragma once

typedef unsigned short header_t;

enum sv_to_cl_state_t
{
	SV_TO_CL_WAITING_STATE,
	SV_TO_CL_READY_STATE,
	SV_TO_CL_PLAYING_STATE
};

enum sv_to_cl_game_over_type_t
{
	SV_TO_CL_NO_WINNER,
	SV_TO_CL_THERE_IS_WINNER
};

enum sv_to_cl_event_t
{
	SV_TO_CL_RUNNER_COL_TO_CACTUS,
	SV_TO_CL_HAIR_GOT_RUNNER,
	SV_TO_CL_RUNNER_GOT_WATER,
	SV_TO_CL_STONE_HIT_RUNNER
};

enum sv_to_cl_t
{
/*	server -> server message header */
	SV_TO_CL_STATE,
	SV_TO_CL_CL_SET,
	SV_TO_CL_CHAT,
	SV_TO_CL_INFO,
	SV_TO_CL_PLAYER_READY,
	SV_TO_CL_BASIC_INFO,
	SV_TO_CL_TILE_ROW,
	SV_TO_CL_GAME_CONTEXT,	// 새로온 클라이언트에게 보내는 context입니다.
	SV_TO_CL_RUNNER_DEAD,
	SV_TO_CL_NEW_OBJECT,
	SV_TO_CL_OBJECT_DEAD,
	SV_TO_CL_ADD_ITEM,
	SV_TO_CL_ERASE_ITEM,
	SV_TO_CL_EVENT,	// miscellaneous events...
	SV_TO_CL_GAME_OVER
};

enum cl_to_sv_event_t
{
	CL_TO_SV_EVENT_UP,
	CL_TO_SV_EVENT_DOWN,
	CL_TO_SV_EVENT_LEFT,
	CL_TO_SV_EVENT_RIGHT,
	CL_TO_SV_EVENT_Q,
	CL_TO_SV_EVENT_W,
	CL_TO_SV_EVENT_E,
	CL_TO_SV_EVENT_R,
	CL_TO_SV_EVENT_READY,
	CL_TO_SV_EVENT_ITEM
};

enum cl_to_sv_t
{
/*	client -> server message header */
	CL_TO_SV_CHAT,
	CL_TO_SV_EVENT,
};

typedef unsigned short runner_id_t;

#define SEO				0u
#define PARK			1u
#define OH				2u
#define JEONG			3u
#define KIM				4u
#define RANDOM_RUNNER	5u
#define NR_RUNNER		6u

enum runner_state_t
{
	RUNNING_STATE,
	Q_SKILL_STATE,
	W_SKILL_STATE,
	E_SKILL_STATE,
	R_SKILL_STATE,
	FALL_DOWN_STATE,
	DEAD_STATE,		// cl side에만 존재
	INVALID_STATE
};

#define BASIC_SAND_STORM_DIST	250
#define BASIC_SAND_STORM_DIST_F	250.f
#define DIST_PER_RUNNER			10
#define DIST_PER_RUNNER_F		10.f


enum object_type_id_t
{
	OBJECT_ITEM,
	OBJECT_STONE,
	OBJECT_HAIR,
	OBJECT_SMOKE,
	OBJECT_INVALID
};

typedef unsigned short	object_id_t;

#define INVALID_OBJECT_ID	static_cast<object_id_t>(-1)

enum item_id_t
{
	ITEM_STONE,
	ITEM_BIG_WONTON,
	ITEM_CURRY_RICE,
	ITEM_DOUBLE_JAJANG,
	ITEM_JAJANG,
	ITEM_JOB_RICE,
	ITEM_JONG_HO_FOOD,
	ITEM_RAMYUN,
	ITEM_RICE,
	ITEM_TEOK_RAMYUN,
	ITEM_WONTON,
	ITEM_INVALID
};
#define MAX_NR_ITEM_PER_RUNNER	3

#include "tile.h"