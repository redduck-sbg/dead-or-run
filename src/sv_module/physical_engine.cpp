#include "physical_engine.h"
#include "../speed_def.h"
#include "../play_scene_layout.h"
#include "../util.h"

/***********************
		public
***********************/

PhysicalEngine::PhysicalEngine()
	: _runners(), _cactus_list(), _stone_list(), _hair_list()
{
}

PhysicalEngine::~PhysicalEngine()
{
}

void PhysicalEngine::Reset(	runner_map_t * runner_map_ptr,
							list<sv_cactus_t> * cactus_list,
							list<sv_item_t> * item_list,
							list<sv_stone_t> * stone_list,
							list<sv_hair_t> * hair_list)
{
	_runners.Clear();
	for(auto iter = runner_map_ptr->Begin();
		iter != runner_map_ptr->End();
		++iter)
	{
		_runners.Insert((*iter).GetKey(),
						taged_runner_t((*iter).GetKey(), true, (*iter).GetEl()) );
	}

	_cactus_list = cactus_list;
	_item_list = item_list;
	_stone_list = stone_list;
	_hair_list = hair_list;
}

void PhysicalEngine::EraseRunner(ID cl_id)
{
	_runners.Erase(cl_id);
}

void PhysicalEngine::Update()
{
	for(auto iter = _runners.Begin(); iter != _runners.End(); ++iter)
	{
		taged_runner_t * trunner = &(*iter).GetEl();
		trunner->rest_time = 1.f;
		if(trunner->runner->GetFlashFlag())
			trunner->rvcity = 50.f * GetUnitVector(trunner->runner->GetDir());
		else
			trunner->rvcity = GetVcity(trunner->runner, sv_delta_time);
	}

	_cur_time = 0.f;

	int cnt = 0;

	col_t c;
	while(PredictFirstCol(&c))
	{
		switch(c.type)
		{
		case RUNNER_COL: HandleRunnerCol(c); break;
		case LEFT_BOUNDARY_COL: HandleLeftBoundaryCol(c); break;
		case RIGHT_BOUNDARY_COL: HandleRightBoundaryCol(c); break;
		case CACTUS_COL: HandleCactusCol(c); break;
		case ITEM_COL: HandleItemCol(c); break;
		case STONE_COL: HandleStoneCol(c); break;
		case HAIR_COL: HandleHairCol(c); break;
		}

		_cur_time = c.time;

		// _cur_time 기준으로 동기화
		for(auto iter = _runners.Begin(); iter != _runners.End(); ++iter)
		{
			SyncToCurTime(&(*iter).GetEl());
		}

		if(++cnt > 8) break;
	}

	for(auto iter = _runners.Begin(); iter != _runners.End(); ++iter)
	{
		taged_runner_t * trunner = &(*iter).GetEl();
		trunner->runner->_pos += trunner->rest_time * trunner->rvcity;
	}

	for(auto iter = _hair_list->begin(); iter != _hair_list->end(); ++iter)
		iter->pos += iter->vcity * (sv_delta_time / MSPPIXEL_F);
	
	for(auto it = _stone_list->begin(); it != _stone_list->end(); ++it)
		it->pos += it->vcity * (sv_delta_time / MSPPIXEL_F);
}

/*
SvRunner * PhysicalEngine::GetRunnerInsideOfCircle(const vector_t & origin, float radius)
{
}
*/

void PhysicalEngine::SetRunnerAir(SvRunner * runner)
{

}

void PhysicalEngine::SetRunnerGround(SvRunner * runner)
{

}

/***********************
		private
***********************/

///////// COLLISION HANDLERS ////////////

void PhysicalEngine::HandleRunnerCol(const col_t & c)
{
	vector_t v = c.trunner1->rvcity;
	vector_t V = c.trunner2->rvcity;
	float m = c.trunner1->runner->GetRadius();
	float M = c.trunner2->runner->GetRadius();

	// wiki 백과 공식입니다
	vector_t v2 = (v * (m - M) + V * 2.0f * M) / (m + M);
	vector_t V2 = (V * (M - m) + v * 2.0f * m) / (m + M);

	// 각 캐릭터의 바야바상태에 따라서 논리가 분기됩니다.
	if(c.trunner1->runner->GetBayabaFlag() &&
		c.trunner2->runner->GetBayabaFlag())
	{
		v2 *= 2.0f;
		V2 *= 2.0f;
		c.trunner1->runner->FallDown(500);
		c.trunner1->runner->ChangeHealth(-30.f);
		c.trunner2->runner->FallDown(500);
		c.trunner2->runner->ChangeHealth(-30.f);
	}
	else if(c.trunner1->runner->GetBayabaFlag() &&
		!c.trunner2->runner->GetBayabaFlag())
	{
		v2 = v;
		V2 *= 1.5f;
		c.trunner2->runner->FallDown(1000);
		c.trunner2->runner->ChangeHealth(-30.f);
	}
	else if(!c.trunner1->runner->GetBayabaFlag() &&
		c.trunner2->runner->GetBayabaFlag())
	{
		v2 *= 1.5f;
		V2 = V;
		c.trunner1->runner->FallDown(1000);
		c.trunner1->runner->ChangeHealth(-30.f);
	}
	else
	{
		// 그리고 각 캐릭터의 점프 상태에 따라서 논리가 분기됩니다.
		if(c.trunner1->runner->_leap_flag && c.trunner2->runner->_leap_flag)
		{
			v2 *= 1.5f;
			V2 *= 1.5f;
			c.trunner1->runner->FallDown(500);
			c.trunner1->runner->ChangeHealth(-15.f);
			c.trunner2->runner->FallDown(500);
			c.trunner2->runner->ChangeHealth(-15.f);
		}
		else if(c.trunner1->runner->_leap_flag && !c.trunner2->runner->_leap_flag)
		{
			v2 = v;
			V2 *= 1.5f;
			c.trunner1->runner->UsingWSkill(1000);
			c.trunner2->runner->ChangeHealth(-10.f);
			c.trunner2->runner->FallDown(500);
		}
		else if(!c.trunner1->runner->_leap_flag && c.trunner2->runner->_leap_flag)
		{
			v2 *= 1.5f;
			V2 = V;
			c.trunner2->runner->UsingWSkill(1000);
			c.trunner1->runner->ChangeHealth(-10.f);
			c.trunner1->runner->FallDown(500);
		}
		
		//충돌 후 에너지 손실 적용...
		v2 *= 0.7f;
		V2 *= 0.7f;
	}

	//충돌하기 직전까지 전진
	c.trunner1->runner->_pos += (c.time - _cur_time) * v;
	c.trunner2->runner->_pos += (c.time - _cur_time) * V;

	//그리고 그들의 남은 시간도 줄어듭니다.
	c.trunner1->rest_time -= (c.time - _cur_time);
	c.trunner2->rest_time -= (c.time - _cur_time);

	//최종적으로 속도 수정... 이후의 일은 이 함수의 역할에서 벗어납니다.
	c.trunner1->rvcity = v2;
	c.trunner2->rvcity = V2;
}

void PhysicalEngine::HandleLeftBoundaryCol(const col_t & c)
{
	c.trunner1->runner->_pos += (c.time - _cur_time) * c.trunner1->rvcity;
	c.trunner1->rvcity = vector_t(ToPositive(c.trunner1->rvcity.x), c.trunner1->rvcity.y);
	c.trunner1->rest_time -= (c.time - _cur_time);
}

void PhysicalEngine::HandleRightBoundaryCol(const col_t & c)
{
	c.trunner1->runner->_pos += (c.time - _cur_time) * c.trunner1->rvcity;
	c.trunner1->rvcity = vector_t(ToNegative(c.trunner1->rvcity.x), c.trunner1->rvcity.y);
	c.trunner1->rest_time -= (c.time - _cur_time);
}

void PhysicalEngine::HandleCactusCol(const col_t & c)
{
	c.trunner1->runner->_pos += (c.time - _cur_time) * c.trunner1->rvcity;
	c.trunner1->runner->ChangeHealth(-5.f);
	c.trunner1->rest_time -= (c.time - _cur_time);

	vector_t dv = c.trunner1->runner->_pos - *c.cactus;

	c.trunner1->rvcity = dv / 2.f;

	Packet send_packet;
	send_packet << static_cast<header_t>(SV_TO_CL_EVENT)
				<< static_cast<header_t>(SV_TO_CL_RUNNER_COL_TO_CACTUS)
				<< c.trunner1->cl_id;
	SafeSendToAll(send_packet);
}

void PhysicalEngine::HandleItemCol(const col_t & c)
{
	if(c.trunner1->runner->CheckInventoryFull()) return;

	item_id_t item_id = c.item->item_id;

	bool erased = false;
	for(auto it = _item_list->begin(); it != _item_list->end(); ++it)
	{
		if(it->id == c.item->id)
		{
			Packet send_packet;
			send_packet << static_cast<header_t>(SV_TO_CL_OBJECT_DEAD)
						<< c.item->id
						<< static_cast<header_t>(OBJECT_ITEM);
			SafeSendToAll(send_packet);
			_item_list->erase(it);
			erased = true;
			break;
		}
	}
	
	assert(erased);

	Packet send_packet;
	send_packet << static_cast<header_t>(SV_TO_CL_ADD_ITEM)
				<< static_cast<header_t>(item_id);
	SafeSend(c.trunner1->cl_id, send_packet);
	c.trunner1->runner->AddItem(item_id);
	
}

void PhysicalEngine::HandleStoneCol(const col_t & c)
{
	c.trunner1->runner->_pos += (c.time - _cur_time) * c.trunner1->rvcity;

	c.trunner1->runner->FallDown(1000);
	c.trunner1->runner->ChangeHealth(-20.f);

	c.trunner1->rest_time -= (c.time - _cur_time);
	c.trunner1->rvcity += c.stone->vcity * (sv_delta_time / MSPPIXEL_F);

	Packet send_packet;
	send_packet << static_cast<header_t>(SV_TO_CL_EVENT)
				<< static_cast<header_t>(SV_TO_CL_STONE_HIT_RUNNER)
				<< c.trunner1->cl_id;
	SafeSendToAll(send_packet);
}

void PhysicalEngine::HandleHairCol(const col_t & c)
{
	c.trunner1->runner->_pos += (c.time - _cur_time) * c.trunner1->rvcity;
	c.trunner1->runner->ChangeHealth(-20.f);
	c.trunner1->rest_time -= (c.time - _cur_time);

	c.trunner1->runner->SetHairFlag(true);
	c.trunner1->runner->FallDown(800);
	c.hair->pos += c.hair->vcity * (sv_delta_time / MSPPIXEL_F) * _cur_time;
	c.hair->vcity = vector_t();
	c.hair->got = true;
	c.hair->grabbed_one = c.trunner1->cl_id;

	c.trunner1->runner->SetPos(c.hair->pos);

	Packet send_packet;
	send_packet << static_cast<header_t>(SV_TO_CL_EVENT)
				<< static_cast<header_t>(SV_TO_CL_HAIR_GOT_RUNNER)
				<< c.trunner1->cl_id;
	SafeSendToAll(send_packet);
}


////////////////// COLLISION PREDICTING /////////////////

void PhysicalEngine::SyncToCurTime(taged_runner_t * trunner)
{
	if(1.f - trunner->rest_time < _cur_time)
	{
		trunner->runner->_pos += (_cur_time - 1.f + trunner->rest_time) * trunner->rvcity;
		trunner->rest_time = 1.f - _cur_time;
	}
}

bool PhysicalEngine::PredictColToLeftBoundary(taged_runner_t * trunner, float * time_ptr)
{
	pos_t next_pos =	trunner->runner->_pos +
						trunner->rest_time * trunner->rvcity;

	const int left_boundary = -OKAWARU_BOX_WIDTH * (SV_MAX_NR_PLAYER / 2);

	if(	next_pos.x - trunner->runner->_radius < left_boundary)
	{
		if(trunner->runner->_pos.x - next_pos.x == 0.f)
			*time_ptr = 0.f;
		else
			*time_ptr = _cur_time +
				trunner->rest_time * ((trunner->runner->_pos.x - (left_boundary + trunner->runner->_radius)) / (trunner->runner->_pos.x - next_pos.x));
		return true;
	}
	else
		return false;
}

bool PhysicalEngine::PredictColToRightBoundary(taged_runner_t * trunner, float * time_ptr)
{
	pos_t next_pos =	trunner->runner->_pos +
						trunner->rest_time * trunner->rvcity;

	const int right_boundary =	OKAWARU_BOX_WIDTH * (SV_MAX_NR_PLAYER / 2);
	if( right_boundary < next_pos.x + trunner->runner->_radius)
	{
		if(next_pos.x - trunner->runner->_pos.x == 0.f)
			*time_ptr = 0.f;
		else
			*time_ptr = _cur_time +
				trunner->rest_time * (((right_boundary - trunner->runner->_radius) - trunner->runner->_pos.x) / (next_pos.x - trunner->runner->_pos.x));
		return true;
	}
	else
		return false;
}

bool PhysicalEngine::PredictColToCactus(	taged_runner_t * trunner,
											sv_cactus_t * cactus,
											float * time_ptr)
{
	vector_t dv = trunner->rvcity;
	vector_t dpos = trunner->runner->GetPos() -
					*cactus;

	float a = dv * dv;
	if(a == 0.f) return false;
	float b = 2.f * (dpos * dv);
	float len = (	trunner->runner->GetRadius() +
					CACTUS_RADIUS_F );
	float c = (dpos * dpos) - (len * len);
	float discriminant = (b * b) - 4.f * a * c;
	
	if(discriminant < POS_EPSILON) return false;

	float t = (-b - sqrtf(discriminant)) / (2.f * a);
	if(t < 0.f || 1.f - _cur_time < t) return false;

	*time_ptr = t + _cur_time;
	return true;
}

bool PhysicalEngine::PredictColToItem(	taged_runner_t * trunner,
										sv_item_t * item,
										float * time_ptr)
{
	vector_t dv = trunner->rvcity;
	vector_t dpos = trunner->runner->GetPos() -
					item->pos;

	float a = dv * dv;
	if(a == 0.f) return false;
	float b = 2.f * (dpos * dv);
	float len = (	trunner->runner->GetRadius() +
					CACTUS_RADIUS_F );
	float c = (dpos * dpos) - (len * len);
	float discriminant = (b * b) - 4.f * a * c;
	
	if(discriminant < POS_EPSILON) return false;

	float t = (-b - sqrtf(discriminant)) / (2.f * a);
	if(t < 0.f || 1.f - _cur_time < t) return false;

	*time_ptr = t + _cur_time;
	return true;
}

bool PhysicalEngine::PredictColToStone(	taged_runner_t * trunner,
										sv_stone_t * stone,
										float * time_ptr)
{
	vector_t dv = trunner->rvcity - (stone->vcity * (sv_delta_time / MSPPIXEL_F));
	vector_t dpos = trunner->runner->GetPos() -
					stone->pos;

	float a = dv * dv;
	if(a == 0.f) return false;
	float b = 2.f * (dpos * dv);
	float len = (	trunner->runner->GetRadius() +
					STONE_RADIUS_F );
	float c = (dpos * dpos) - (len * len);
	float discriminant = (b * b) - 4.f * a * c;
	
	if(discriminant < POS_EPSILON) return false;

	float t = (-b - sqrtf(discriminant)) / (2.f * a);
	if(t < 0.f || 1.f - _cur_time < t) return false;

	*time_ptr = t + _cur_time;
	return true;
}

bool PhysicalEngine::PredictColToHair(	taged_runner_t * trunner,
										sv_hair_t * hair,
										float * time_ptr)
{
	vector_t dv = trunner->rvcity - (hair->vcity * (sv_delta_time / MSPPIXEL_F));
	vector_t dpos = trunner->runner->GetPos() -
					hair->pos;

	float a = dv * dv;
	if(a == 0.f) return false;
	float b = 2.f * (dpos * dv);
	float len = ( trunner->runner->GetRadius() + 13.f);
	float c = (dpos * dpos) - (len * len);
	float discriminant = (b * b) - 4.f * a * c;
	
	if(discriminant < POS_EPSILON) return false;

	float t = (-b - sqrtf(discriminant)) / (2.f * a);
	if(t < 0.f || 1.f - _cur_time < t) return false;

	*time_ptr = t + _cur_time;
	return true;
}

bool PhysicalEngine::PredictCol(	taged_runner_t * trunner1,
									taged_runner_t * trunner2,
									float * time_ptr)
{
	vector_t dv = trunner1->rvcity - trunner2->rvcity;
	vector_t dpos = trunner1->runner->GetPos() -
					trunner2->runner->GetPos();

	float a = dv * dv;
	if(a == 0.f) return false;
	float b = 2.f * (dpos * dv);
	float len = (	trunner1->runner->GetRadius() +
					trunner2->runner->GetRadius() );
	float c = (dpos * dpos) - (len * len);
	float discriminant = (b * b) - 4.f * a * c;
	
	if(discriminant < POS_EPSILON) return false;

	float t = (-b - sqrtf(discriminant)) / (2.f * a);
	if(t < 0.f || 1.f - _cur_time < t) return false;

	*time_ptr = t + _cur_time;
	return true;
}

bool PhysicalEngine::PredictFirstCol(col_t * col_ptr)
{
	bool		first_found = true;

	float time;
	for(auto iter = _runners.Begin(); iter != _runners.End(); ++iter)
	{
		taged_runner_t * trunner1 = &(*iter).GetEl();
		// 왼쪽 경계 충돌 예측 검사
		if(trunner1->runner->GetHairFlag() == false)
		{
			if(PredictColToLeftBoundary(trunner1, &time))
			{
				if(first_found || col_ptr->time < time)
				{
					col_ptr->type = LEFT_BOUNDARY_COL;
					col_ptr->trunner1 = trunner1;
					col_ptr->time = time;
					first_found = false;
				}
			}
		
			// 오른쪽 경계 충돌 예측 검사
			if(PredictColToRightBoundary(trunner1, &time))
			{
				if(first_found || col_ptr->time < time)
				{
					col_ptr->type = RIGHT_BOUNDARY_COL;
					col_ptr->trunner1 = trunner1;
					col_ptr->time = time;
					first_found = false;
				}
			}
		}

		// 선인장과 충돌 예측 검사
		if(trunner1->runner->_leap_flag == false &&
			trunner1->runner->GetFlashFlag() == false &&
			trunner1->runner->GetGhostFlag() == false)
		{
			for(auto iter2 = _cactus_list->begin(); iter2 != _cactus_list->end(); ++iter2)
			{
				if(PredictColToCactus(trunner1, &*iter2, &time))
				{
					if(first_found || time < col_ptr->time)
					{
						col_ptr->type = CACTUS_COL;
						col_ptr->trunner1 = trunner1;
						col_ptr->cactus = &*iter2;
						col_ptr->time = time;
						first_found = false;
					}
				}
			}
		}

		// 길 위에 있는 아이템과 충돌 예측 검사
		if(!trunner1->runner->CheckInventoryFull() && trunner1->runner->_leap_flag == false)
		{
			for(auto iter2 = _item_list->begin(); iter2 != _item_list->end(); ++iter2)
			{
				if(PredictColToItem(trunner1, &*iter2, &time))
				{
					if(first_found || time < col_ptr->time)
					{
						col_ptr->type = ITEM_COL;
						col_ptr->trunner1 = trunner1;
						col_ptr->item = &*iter2;
						col_ptr->time = time;
						first_found = false;
					}
				}
			}
		}

		// 짱돌과 충돌 예측 검사
		if(trunner1->runner->GetState() != FALL_DOWN_STATE &&
			trunner1->runner->GetFlashFlag() == false &&
			trunner1->runner->GetGhostFlag() == false)
		{
			for(auto iter2 = _stone_list->begin(); iter2 != _stone_list->end(); ++iter2)
			{
				if(trunner1->cl_id == iter2->owner_id) continue;

				if(PredictColToStone(trunner1, &*iter2, &time))
				{
					if(first_found || time < col_ptr->time)
					{
						col_ptr->type = STONE_COL;
						col_ptr->trunner1 = trunner1;
						col_ptr->stone = &*iter2;
						col_ptr->time = time;
						first_found = false;
					}
				}
			}
		}

		if(!trunner1->runner->GetHairFlag() &&
			trunner1->runner->GetFlashFlag() == false &&
			trunner1->runner->GetGhostFlag() == false)
		{
			// 머리카락과 충돌 예측 검사
			for(auto iter2 = _hair_list->begin(); iter2 != _hair_list->end(); ++iter2)
			{
				if(iter2->got || iter2->owner_id == trunner1->cl_id) continue;
				if(PredictColToHair(trunner1, &*iter2, &time))
				{
					if(first_found || time < col_ptr->time)
					{
						col_ptr->type = HAIR_COL;
						col_ptr->trunner1 = trunner1;
						col_ptr->hair = &*iter2;
						col_ptr->time = time;
						first_found = false;
					}
				}
			}
		}

		// runner간의 충돌 예측 검사
		for(auto iter2 = _runners.Begin(); iter2 != iter; ++iter2)
		{
			taged_runner_t * trunner2 = &(*iter2).GetEl();

			if(trunner1->runner->GetState() == FALL_DOWN_STATE ||
				trunner2->runner->GetState() == FALL_DOWN_STATE ||
				trunner1->runner->GetFlashFlag() ||
				trunner2->runner->GetFlashFlag() ||
				trunner1->runner->GetGhostFlag() ||
				trunner2->runner->GetGhostFlag()) continue;

			if(PredictCol(trunner1, trunner2, &time))
			{
				if(first_found || time < col_ptr->time)
				{
					col_ptr->type = RUNNER_COL;
					col_ptr->trunner1 = trunner1;
					col_ptr->trunner2 = trunner2;
					col_ptr->time = time;
					first_found = false;
				}
			}
		}
	}

	return !first_found;
}