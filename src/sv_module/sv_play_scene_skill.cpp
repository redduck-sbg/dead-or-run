#include "sv_play_scene.h"

void SvPlayScene::HandleBackAttackSkill(SvRunner * runner)
{
	if(runner->GetState() == FALL_DOWN_STATE) return;

	if(runner->GetHealth() > 15.f) runner->ChangeHealth(-15.f);
	else return;

	runner->UsingQSkill(500);

	// hit circle
	vector_t	hit_origin =	runner->GetPos() +
								GetUnitVector(runner->GetDir()) * 25.f;
	float		hit_radius = 30.f;

	for(auto iter = _runner_map.Begin(); iter != _runner_map.End(); ++iter)
	{
		SvRunner * cur_runner = (*iter).GetEl();
		if(cur_runner == runner) continue;
		if(GetDistance(cur_runner->GetPos(), hit_origin) < hit_radius)
		{
			cur_runner->FallDown(1000);
			cur_runner->ChangeHealth(-10.f);
		}
	}
}

void SvPlayScene::HandleMadDogSkill(SvRunner * runner)
{
	if(runner->GetState() == FALL_DOWN_STATE) return;

	if(runner->GetHealth() > 35.f) runner->ChangeHealth(-35.f);
	else return;

	runner->UsingQSkill(7200);

	runner->SetSpeed(41.f);
}

void SvPlayScene::HandleBayabaSkill(SvRunner * runner)
{
	if(runner->GetState() == FALL_DOWN_STATE) return;

	if(runner->GetHealth() > 55.f) runner->ChangeHealth(-55.f);
	else return;
	
	runner->UsingQSkill(120);
	
	runner->SetSpeed(200.f);

	runner->SetBayabaFlag();
}

void SvPlayScene::HandleGrudgeSkill(ID cl_id, SvRunner * runner)
{
	if(runner->GetState() == FALL_DOWN_STATE) return;

	if(runner->GetHealth() > 35.f) runner->ChangeHealth(-35.f);
	else return;

	runner->UsingQSkill(HAIR_FLY_TIME);

	pos_t pos(runner->GetPos() + GetUnitVector(runner->GetDir()) * 10.f);
	vector_t vcity(	GetUnitVector(runner->GetDir())
					* (HAIR_SPEED_F + runner->GetSpeed()) );

	sv_hair_t new_hair(	_object_id_cnt,
						cl_id,
						pos,
						vcity,
						false,
						-1,
						sv_present_time);

	_hair_list.push_back(new_hair);

	Packet send_packet;
	send_packet << static_cast<header_t>(SV_TO_CL_NEW_OBJECT)
				<< _object_id_cnt
				<< static_cast<header_t>(OBJECT_HAIR)
				<< cl_id
				<< pos.x
				<< pos.y
				<< vcity.x
				<< vcity.y;
	SafeSendToAll(send_packet);

	++_object_id_cnt;
}

void SvPlayScene::HandleSmokeSkill(ID cl_id, SvRunner * runner)
{
	if(runner->GetState() == FALL_DOWN_STATE) return;

	if(runner->GetHealth() > 20.f) runner->ChangeHealth(-20.f);
	else return;

	runner->UsingWSkill(300);

	sv_smoke_t smoke(	_object_id_cnt,
						cl_id,
						runner->GetPos(),
						(runner->GetSpeed() + SMOKE_SPEED_F) * GetUnitVector(runner->GetDir()),
						sv_present_time );

	_smoke_list.push_back(smoke);

	Packet send_packet;
	send_packet << static_cast<header_t>(SV_TO_CL_NEW_OBJECT)
				<< smoke.id
				<< static_cast<header_t>(OBJECT_SMOKE)
				<< smoke.owner_id
				<< smoke.pos.x
				<< smoke.pos.y
				<< smoke.vcity.x
				<< smoke.vcity.y;
	SafeSendToAll(send_packet);

	++_object_id_cnt;
}

void SvPlayScene::HandleLeapSkill(SvRunner * runner)
{
	if(runner->GetState() == FALL_DOWN_STATE) return;

	if(runner->GetHealth() > 50.f) runner->ChangeHealth(-50.f);
	else return;

	runner->UsingWSkill(1500);

	runner->SetSpeed(55.f);

	runner->SetLeapFlag();
}

void SvPlayScene::HandleFlashTransfer(SvRunner * runner)
{
	if(runner->GetState() == FALL_DOWN_STATE) return;

	if(runner->GetHealth() > 30.f) runner->ChangeHealth(-30.f);
	else return;

	runner->UsingWSkill(200);
	runner->SetFlashFlag(true);
}

void SvPlayScene::HandleGodComeSkill(SvRunner * runner)
{
	if(runner->GetState() == FALL_DOWN_STATE) return;

	if(runner->GetHealth() > 50.f) runner->ChangeHealth(-50.f);
	else return;

	runner->UsingQSkill(5000);
}

void SvPlayScene::HandleGhostSkill(SvRunner * runner)
{
	if(runner->GetState() == FALL_DOWN_STATE) return;

	if(runner->GetHealth() > 30.f) runner->ChangeHealth(-30.f);
	else return;

	runner->UsingWSkill(1500);

	runner->SetGhostFlag(true);
}

void SvPlayScene::HandleSkillEvent(ID cl_id, 
									  const sv_skill_id_t & skill_id,
									  SvRunner * runner)
{
	if(skill_id == SV_INVALID_SKILL) return;

	switch(skill_id)
	{
	case SV_BACK_ATTACK_SKILL: HandleBackAttackSkill(runner); break;
	case SV_MAD_DOG_SKILL: HandleMadDogSkill(runner); break;
	case SV_BAYABA_SKILL: HandleBayabaSkill(runner); break;
	case SV_GRUDGE_SKILL: HandleGrudgeSkill(cl_id, runner); break;
	case SV_SMOKE_SKILL: HandleSmokeSkill(cl_id, runner); break;
	case SV_LEAP_SKILL: HandleLeapSkill(runner); break;
	case SV_FLASH_TRANSFER: HandleFlashTransfer(runner); break;
	case SV_GOD_COME_SKILL: HandleGodComeSkill(runner); break;
	case SV_GHOST_SKILL: HandleGhostSkill(runner); break;
	}
}
