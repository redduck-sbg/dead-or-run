/*OOPARTS OOPARTS OOPARTS OOPARTS OOPARTS OOPARTS OOPARTS
=========================================================
	sv_skills.h

	기술들을 선언했습니다.
=======================================================*/

#pragma once

enum sv_skill_id_t
{
	SV_BACK_ATTACK_SKILL,
	SV_LEAP_SKILL,
	SV_BAYABA_SKILL,
	SV_MAD_DOG_SKILL,
	SV_SMOKE_SKILL,
	SV_GRUDGE_SKILL,
	SV_FLASH_TRANSFER,
	SV_GOD_COME_SKILL,
	SV_GHOST_SKILL,
	SV_INVALID_SKILL
};