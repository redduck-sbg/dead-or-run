/*OOPARTS OOPARTS OOPARTS OOPARTS OOPARTS OOPARTS OOPARTS
=========================================================
	char_desc.h

	custom SFML CharDesc class�� header file �Դϴ�.
=======================================================*/

#pragma once

#include "internal.h"

class CharDesc : public Drawable
{
public:
	CharDesc();

	void Reset();
	void Update(const wchar_t * face_name, const wchar_t * desc);

private:
	virtual void draw(RenderTarget& target, RenderStates states) const;

private:
	Sprite						_face;
	Text						_desc_text;
	RectangleShape				_desc_text_bg;
};