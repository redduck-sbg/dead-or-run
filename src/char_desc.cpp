#include "char_desc.h"
#include "res.h"
#include "util.h"
#include "play_scene_layout.h"

CharDesc::CharDesc() :
	_face(),
	_desc_text(L"...", normal_font, 15U),
	_desc_text_bg()
{
	_desc_text.setColor(Color(255, 255, 255));

	_desc_text_bg.setSize(Vector2f(325.f, 90.f));
	_desc_text_bg.setPosition(	static_cast<float>(window.getSize().x - VEHUMET),
								static_cast<float>(SHINING_ONE + 170.f + TROG));
	_desc_text_bg.setOrigin(325.f, 0.f);
	_desc_text_bg.setFillColor(Color(255, 0, 0, 127));
}

void CharDesc::Reset()
{
	Update(L"unknown-face", L"...");
}

void CharDesc::Update(const wchar_t * face_name, const wchar_t * desc)
{
	_face = *sprite_map[face_name];
	_face.setOrigin(0.f, 0.f);
	SetScaleToSize(&_face, 200.f, 170.f);
	_face.setPosition(	static_cast<float>(window.getSize().x - VEHUMET - 200), 
						static_cast<float>(SHINING_ONE));

	_desc_text.setString(desc);
	_desc_text.setOrigin(325.f, 0.f);
	_desc_text.setPosition(	static_cast<float>(window.getSize().x - VEHUMET),
							static_cast<float>(SHINING_ONE + 170.f + TROG));
}

void CharDesc::draw(RenderTarget& target, RenderStates states) const
{
	target.draw(_face, states);	
	target.draw(_desc_text, states);
	target.draw(_desc_text_bg, states);
}